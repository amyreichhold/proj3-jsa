"""
Flask web site with vocabulary matching game
(identify vocabulary words that can be made 
from a scrambled string)
"""

import flask
import logging

# Our own modules
from letterbag import LetterBag
from vocab import Vocab
from jumble import jumbled
import config

###
# Globals
###
app = flask.Flask(__name__)

CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY  # Should allow using session variables

#
# One shared 'Vocab' object, read-only after initialization,
# shared by all threads and instances.  Otherwise we would have to
# store it in the browser and transmit it on each request/response cycle,
# or else read it from the file on each request/responce cycle,
# neither of which would be suitable for responding keystroke by keystroke.

WORDS = Vocab(CONFIG.VOCAB)

###
# Pages
###


@app.route("/")
#@app.route("/index")
def index():
    """The main page of the application"""
    flask.g.vocab = WORDS.as_list()
    flask.session["target_count"] = min(
        len(flask.g.vocab), CONFIG.SUCCESS_AT_COUNT)
    flask.session["jumble"] = jumbled(
        flask.g.vocab, flask.session["target_count"])
    flask.session["matches"] = []
    app.logger.debug("Session variables have been set")
    assert flask.session["matches"] == []
    assert flask.session["target_count"] > 0
    app.logger.debug("At least one seems to be set correctly")
    return flask.render_template('vocab.html')

@app.route("/success")
def success():
    return flask.render_template('success.html')

#######################
# Form handler.
# CIS 322 note:
#   You'll need to change this to a
#   a JSON request handler
#######################

@app.route("/_check")
def check():
    # The data we need, from form and from cookie
    jumble = flask.session["jumble"]
    matches = flask.session.get("matches", [])  # Default to empty list
    text = flask.request.args.get("text", type=str)

    # Is it good?
    in_jumble = LetterBag(jumble).contains(text)
    matched = WORDS.has(text)
    message = ""
    results = ""
    case = -1

    # Respond appropriately
    if matched and in_jumble and not (text in matches):
        case = 1
        # Cool, they found a new word
        matches.append(text)
        flask.session["matches"] = matches
        results = """<h2>You found </h2>
                    <p id=\"results\">

                    {matches}
                    </p>""".format(matches="\n".join(matches))
    elif text in matches:
        case = 2
        message = """<p class =\"message\"> You already found {}</p>""".format(text)
        #message = "You already found {}".format(text)
    elif not matched:
        case = 3
        message = """<p class =\"message\"> {} isn't in the list of words</p>""".format(text)
        #message = "{} isn't in the list of words".format(text)
    elif not in_jumble:
        case = 4
        message = """<p class =\"message\"> {} can\'t be made form the letters {}</p>""".format(text, jumble)
        #message = '"{}" can\'t be made from the letters {}'.format(text, jumble)
    else:
        case = 5
        app.logger.debug("This case shouldn't happen!")
        assert False  # Raises AssertionError

    # Choose page:  Solved enough, or keep going?
    if len(matches) >= flask.session["target_count"]:
        success = True
        keep_going = False
    else:
        success = False
        keep_going = True

    rslt = {"results": results, "matched": matched, "jumble": jumble, "in_jumble": in_jumble, "message": message, "success": success, "url_for_success": flask.url_for("success"), "case": case}
    return flask.jsonify(result=rslt)

###############
# AJAX request handlers
#   These return JSON, rather than rendering pages.
###############

@app.route("/_example")
def example():
    """
    Example ajax request handler
    """
    app.logger.debug("Got a JSON request")
    rslt = {"key": "value"}
    return flask.jsonify(result=rslt)


#################
# Functions used within the templates
#################

@app.template_filter('filt')
def format_filt(something):
    """
    Example of a filter that can be used within
    the Jinja2 code
    """
    return "Not what you asked for"

###################
#   Error handlers
###################


@app.errorhandler(404)
def error_404(e):
    app.logger.warning("++ 404 error: {}".format(e))
    return flask.render_template('404.html'), 404


@app.errorhandler(500)
def error_500(e):
    app.logger.warning("++ 500 error: {}".format(e))
    assert not True  # I want to invoke the debugger
    return flask.render_template('500.html'), 500


@app.errorhandler(403)
def error_403(e):
    app.logger.warning("++ 403 error: {}".format(e))
    return flask.render_template('403.html'), 403


####

if __name__ == "__main__":
    if CONFIG.DEBUG:
        app.debug = True
        app.logger.setLevel(logging.DEBUG)
        app.logger.info(
            "Opening for global access on port {}".format(CONFIG.PORT))
        app.run(port=CONFIG.PORT, host="0.0.0.0")
